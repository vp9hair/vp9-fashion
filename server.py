from flask import Flask, jsonify, request, session, flash, url_for, render_template, redirect, abort
from flask_cors import CORS
from werkzeug.utils import secure_filename
from flask_session import Session

from datetime import timedelta
from time import time
import os
import pycurl_requests as requests
# import requests
from flask_sqlalchemy import SQLAlchemy
from config import (
    SECRET_KEY,
    SWAP_URL,
    MAIL_PASSWORD,
    MAIL_PORT,
    MAIL_SERVER,
    MAIL_USERNAME,
    SQLALCHEMY_DATABASE_URI,
    SERVER_PORT,
    SERVER_NAME
)
from google.oauth2 import id_token
from google.auth.transport import requests as rq
import json
from pip._vendor import cachecontrol
# import google.auth.transport.urllib3
from google_auth_oauthlib.flow import Flow
# from google.oauth2 import id_token
import pathlib
import urllib3
import logging
from uuid import uuid4
# logging.basicConfig(filename='/var/log/vp9-fashion/server.log', level=logging.DEBUG)
logging.basicConfig(level=logging.WARNING)

log = logging.getLogger("vp9-fashion")

server = Flask(__name__)
CORS(server)
server.secret_key = SECRET_KEY

server.config['PERMANENT_SESSION_LIFETIME'] =  timedelta(minutes=5)

server.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
db = SQLAlchemy(server)

class user(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80))
    email = db.Column(db.String(120))
    password = db.Column(db.String(80))

os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"
GOOGLE_CLIENT_ID = "585429990064-u48t6el5n5cpk3srjipdr53jt88jgpq4.apps.googleusercontent.com"
client_secrets_file = os.path.join(pathlib.Path(__file__).parent, "secret_key.json")

flow = Flow.from_client_secrets_file(
    client_secrets_file=client_secrets_file,
    scopes=["https://www.googleapis.com/auth/userinfo.profile", "https://www.googleapis.com/auth/userinfo.email", "openid"],
    redirect_uri=f"{SERVER_NAME}/callback"
)

#token that can be generated talking with @BotFather on telegram
from telegram import Bot

my_token = '1514441067:AAFA-gHWCv0xS4lt63AZ4DSi1uHaLIKjW18'
chat_id = '-574064882'
def tele_send(msg, chat_id=chat_id, token=my_token):
    bot = Bot(token=token)
    bot.sendMessage(chat_id=chat_id, text=msg)

def login_is_required(function):
    def wrapper(*args, **kwargs):
        if "google_id" not in session:
            return abort(401)  # Authorization required
        else:
            return function()

    return wrapper


@server.route("/googlelogin")
def testlogin():
    authorization_url, state = flow.authorization_url()
    session["state"] = state
    # log.warning(authorization_url)
    return redirect(authorization_url)


@server.route("/callback")
def callback(): 
    try:
        flow.fetch_token(authorization_response=request.url)

        if not session["state"] == request.args["state"]:
            abort(500)  # State does not match!
        http = urllib3.PoolManager()
        credentials = flow.credentials
        # request_session = requests.session()
        # cached_session = cachecontrol.CacheControl(request_session)
        # token_request = rq(session=cached_session)
        # token_request = google.auth.transport.urllib3.Request(http)
        token_request = rq.Request(requests.session())
        id_info = id_token.verify_oauth2_token(
            id_token=credentials._id_token,
            request=token_request,
            audience=GOOGLE_CLIENT_ID
        )
        session["google_id"] = id_info.get("sub")
        session["name"] = id_info.get("name")
        session['login'] = 1
        session['email'] = id_info.get("email")
    except Exception as e:
        print("BUGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG",str(e))


    return redirect("/")


@server.route("/logout")
def testlogout():
    session.clear()
    return redirect("/home")


# @server.route("/protected_area")
# # @login_is_required
# def protected_area():
#     return f"Hello {session['name']}! <br/> <a href='/testlogout'><button>Logout</button></a>"

@server.route("/home/")
def homepage():
    session.permanent = True
    return render_template("homepage.html")

@server.route("/tokensignin", methods=["POST"])
def tokensignin():
    session.permanent = True
    if request.method == "POST":
        CLIENT_ID = '585429990064-u48t6el5n5cpk3srjipdr53jt88jgpq4.apps.googleusercontent.com'
        data = json.loads(request.data)
        client_token = data.get('idtoken')
        try:
            # Specify the CLIENT_ID of the app that accesses the backend:
            idinfo = id_token.verify_oauth2_token(client_token, rq.Request(), CLIENT_ID)

            session["login"] = 1
            session['email'] = idinfo.get("email")
            print('Sussesful authorize!')
            return {"authorize": True}
        except ValueError:
            # Invalid token
            print('Invalid token!')
            abort(401, "Google sign-in failed.")


@server.route("/login",methods=["GET", "POST"])
def login():
    session.permanent = True
    if request.method == "POST":
        uname = request.form["uname"]
        passw = request.form["passw"]
        
        login = user.query.filter_by(username=uname, password=passw).first()
        if login is not None:
            session["login"] = 1
            session['email'] = login.email
            return redirect('/')
    return render_template("login.html")


from flask_mail import Mail, Message

mail= Mail(server)

server.config['MAIL_SERVER']= MAIL_SERVER
server.config['MAIL_PORT'] = MAIL_PORT
server.config['MAIL_USERNAME'] = MAIL_USERNAME
server.config['MAIL_PASSWORD'] = MAIL_PASSWORD
server.config['MAIL_USE_TLS'] = False
server.config['MAIL_USE_SSL'] = True
mail = Mail(server)

import string
import random

@server.errorhandler(404)
def not_found(e): 
    # defining function 
    return render_template("404.html") 



def gen_code(length=5):
    randomstr = ''.join(random.choices(string.ascii_letters+string.digits,k=length))
    return randomstr

def sent(recipients, code):
    msg = Message('Verify-No-reply', sender = MAIL_USERNAME, recipients = [recipients])
    msg.body = f"Hello, welcome to Face-Hair Fashion \n Your code: {code}"
    mail.send(msg)
    return "Sent"

def sentfile(recipients, list_file_path):
    msg = Message('Nonavel-Result-No-reply', sender = MAIL_USERNAME, recipients = [recipients])
    msg.body = f"Hello, welcome to Face-Hair Fashion \n Your Result: file attach bellow here"
    for index, j in enumerate(list_file_path):
        with server.open_resource(j) as fp:
            msg.attach(f"img_{index}.jpg", "image/jpg", fp.read())
    mail.send(msg)
    return "Sent"

@server.route("/register", methods=["GET", "POST"])
def register():
    session.permanent = True
    if request.method == "POST":
        uname = request.form['uname']
        mail = request.form['mail']
        passw = request.form['passw']
        #email
        code = gen_code()
        sent(mail, code)

        session["code"] = code
        session["mail"] = mail
        session["uname"] = uname
        session["passw"] = passw
        return redirect('/verified-status')
    return render_template("register.html")

@server.route("/verified-status/", methods =["GET","POST"])
def verify():
    session.permanent = True
    if request.method == "POST":
        if "code" in session:
            if request.form["code-verify"] == session["code"]:
                uname = session["uname"]
                mail = session["mail"]
                passw = session["passw"]
                
                with open('db_email.txt','a+') as f:
                    f.write('\n'+str(mail))

                register = user(username = uname, email = mail, password = passw)
                db.session.add(register)
                db.session.commit()

                return redirect(url_for("login"))
    if request.method == "GET":
        return render_template("popup.html")

@server.route('/hello/', methods=["GET"])
def hello():
    session.permanent = True
    if "login" in session and session["login"] == 1:
        return 'hello everyone'
    else:
        return "Vui long login"


def adjust_style(len_folder_imgs):
    with open("./static/style/styles.css", "a") as f:
        element = "#imgs"+str(len_folder_imgs)+"""{
                        border-radius: 30px;
                        border: 10px solid transparent;
                        border-color: #fff;
                        }"""
        f.write("\n"+element)

@server.route('/api/', methods = ["POST"])
def api():
    session.permanent = True
    if "login" in session and session["login"] == 1:
        if request.method == "POST":
            value = request.values["position"]
            return jsonify(data={
                "position": value,
            })
    return False


from shutil import copyfile
@server.route('/', methods=["GET", "POST", "PUT"])
# @login_is_required
def index():
    session.permanent = True
    if "login" in session and session["login"] == 1:
        path = './static/imgs'
        SizeFolder = len(os.listdir(path))
        if request.method == "GET":
            log.warning("user")
            log.warning(session.get("email"))
            # request.remote_addr
            user_email = session.get("email")
            imgs_client_path = os.path.join("./static/client",user_email)
            log.warning(imgs_client_path)
            ## default albums
            default_albums_path = "./static/default_albums"
            # print(os.listdir(default_albums_path))
            albums_list = os.listdir(default_albums_path)
            # albums = [(i + 1, f"{default_albums_path}/{j}") for i,j in enumerate(albums_list)]
            album_details = {}
            for al in albums_list:
                album_path = os.path.join(default_albums_path, al)
                album_details[album_path] = os.listdir(album_path)
            # print(album_details)

            if not os.path.exists(imgs_client_path):
                os.mkdir(imgs_client_path)

            for file_img in os.listdir(path):
                x = os.path.join(path, file_img)
                if file_img in os.listdir(imgs_client_path):
                    continue
                else:
                    copyfile(x, imgs_client_path+'/'+file_img)

            path = imgs_client_path
            # print(path)
            SizeFolder = len(os.listdir(path))
            # print(SizeFolder)
            imgs = os.listdir(path)
            faces = [[i+1,f'{path}/{j}'] for i,j in enumerate(imgs)]
            # print(faces)
            session.permanent = True
            session["faces"] = faces
            session['album_details'] = album_details
            return render_template('index.html',
                                    faces=faces,
                                    sizeFolder=SizeFolder,
                                    imgs_client_path=imgs_client_path,
                                    sizeFolderOrigin=len(os.listdir("./static/imgs/")),
                                    # albums=albums,
                                    album_details=album_details)
        
        try:
            if "email" not in request.form:
                if request.method == "POST" and request.files["adding_image"]:
                    log.warning("user")
                    log.warning(session.get("email"))
                    # request.remote_addr
                    user_email = session.get("email")
                    imgs_client_path = os.path.join("./static/client",user_email)
                    if not os.path.exists(imgs_client_path):
                        os.mkdir(imgs_client_path)

                    log.warning("PUTTTTTTTTTTT")
                    files = request.files.getlist("adding_image")
                    # check len directory imgs
                    # path_imgs = "./static/imgs"
                    path_imgs = imgs_client_path
                    len_folder_imgs = len(os.listdir(path_imgs))
                    # increase files in imgs folder added
                    for i in files:
                        len_folder_imgs+=1
                        name_img = "img_" + str(len_folder_imgs) + '.jpg'
                        img_path = os.path.join(path_imgs,name_img)
                        i.save(img_path)
                        content = requests.post(f'{SWAP_URL}/api/crop-face/?img_path={img_path[2:]}')
                        adjust_style(len_folder_imgs)
                        # adjust styles.css
                    # reload_folder(path_name=imgs_client_path, dst_finally="hello")
                    return redirect("/")
        except Exception as e:
            log.warning(e)
            pass
        try:
            if not request.form["email"]:
                if request.method == "POST" and not request.files["adding_image"] and not request.files["input_phone"]:
                    if not request.form["email"]:
                        log.warning('this is upload post')
                        log.warning(request.form)
                        log.warning("user")
                        log.warning(session.get("email"))
                        # request.remote_addr
                        user_email = session.get("email")
                        imgs_client_path = os.path.join("./static/client",user_email)
                        if not os.path.exists(imgs_client_path):
                            os.mkdir(imgs_client_path)

                        sizeFolder = len(os.listdir(imgs_client_path))
                        url_displays = []
                        folder = request.files.getlist("directory")
                        
                        files_name = folder[0].filename
                        # rename_folder
                        if 'uploadedfolder' in request.form:
                            folder_name = request.form.get('uploadedfolder')
                        else:    
                            folder_name = files_name.split('/')[0]
                        
                        try:
                            if not os.path.exists(f'./static/uploaded/{folder_name}'):
                                os.mkdir(f'./static/uploaded/{folder_name}')
                            else:
                                folder_name = folder_name+'_'+str(time())
                                os.mkdir(f'./static/uploaded/{folder_name}')
                        except Exception as e:
                                log.warning(e)
                        for i in folder:
                            name_i = i.filename
                            
                            if i.content_type == 'image/jpeg' or i.content_type == "image/png":
                                x = f'./static/uploaded/{folder_name}/{name_i.split("/")[-1]}'
                                url_displays.append(x[1:])
                                i.save(x)
                            else:
                                print(i.content_type)
                            
                        folder_display = f'./static/uploaded/{folder_name}'        
                                
                        if "faces" in session:
                            faces = session["faces"]
                        album_details = session.get("album_details", {})
                        return render_template('index.html',
                                               faces=faces,
                                               url_displays=url_displays,
                                               sizeFolder=sizeFolder,
                                               imgs_client_path=imgs_client_path,
                                               sizeFolderOrigin=len(os.listdir("./static/imgs/")),
                                               album_details=album_details,
                                               folder_display=folder_display)

            elif request.form["phone"]:
                try:
                    phone_number = request.form.get('phone', 'error_number')
                    email = session.get('email', "")
                    name = session.get('name', '')
                    message = '''Name: {}\nEmail: {}\nPhone: {}'''.format(name, email, phone_number)
                    tele_send(message)
                    return redirect(url_for('index', alert_message="Sent Successfull"))

                except Exception as e:
                    log.warning(e)
                # try:
                #     # email_name = request.form["email"]
                #     email_name = session.get('email')
                #     if "folder" in session:
                #         folder = session["folder"]
                #         print(f'{SWAP_URL}/api/via-email/?folder_name={folder}')
                #         log.warning(folder)
                #         content = requests.post(f'{SWAP_URL}/api/via-email/?folder_name={folder}')
                #         list_attach_files = content.json()["data"]["imgs"]
                #         sentfile(email_name, list_attach_files)
                #         return redirect(url_for('index', alert_message="Sent Successfull"))
                # except Exception as e:
                #     log.warning(e)

        except Exception as e:
            log.warning(e)
            if request.method == "POST" and "input_phone" in request.files:
                log.warning("user")
                log.warning(session.get("email"))
                # request.remote_addr
                user_email = session.get("email")
                imgs_client_path = os.path.join("./static/client",user_email)
                if not os.path.exists(imgs_client_path):
                    os.mkdir(imgs_client_path)

                sizeFolder = len(os.listdir(imgs_client_path))
                url_displays = []
                folder = request.files.getlist("input_phone")
                folder_name = str(uuid4())

                session.permanent = True
                session["folder"] = folder_name

                try:
                    if not os.path.exists(f'./static/uploaded/{folder_name}'):
                        os.mkdir(f'./static/uploaded/{folder_name}')
                    else:
                        folder_name = folder_name+'_'+str(time())
                        os.mkdir(f'./static/uploaded/{folder_name}')
                except Exception as e:
                        log.warning(e)
                for i in folder:
                    name_i = i.filename
                    
                    if i.content_type == 'image/jpeg' or i.content_type == "image/png":
                        x = f'./static/uploaded/{folder_name}/{name_i.split("/")[-1]}'
                        url_displays.append(x[1:])
                        i.save(x)
                    else:
                        print(i.content_type)
                    
                folder_display = f'./static/uploaded/{folder_name}'        
                root_folder = f"static/uploaded/{folder_name}"        
                if "faces" in session:
                    faces = session.get("faces", [])
                
                album_details = session.get("album_details", {})
                    
                return render_template('index.html',
                                        faces=faces,
                                        url_displays=url_displays,
                                        sizeFolder = sizeFolder,
                                        imgs_client_path=imgs_client_path,
                                        sizeFolderOrigin=len(os.listdir("./static/imgs/")),
                                        folder=root_folder,
                                        album_details=album_details,
                                        folder_display=folder_display)

            elif request.method == "POST" and "input_phone" not in request.files and "email" not in request.form and "adding_image" not in request.files:
                log.warning("user")
                log.warning(session.get("email"))
                # print('request.files xxxxxxxxxxxxxxxxxxxxxxx', request.files)
                # request.remote_addr
                user_email = session.get("email")
                imgs_client_path = os.path.join("./static/client",user_email)
                if not os.path.exists(imgs_client_path):
                    os.mkdir(imgs_client_path)

                sizeFolder = len(os.listdir(imgs_client_path))
                url_displays = []

                if "default_album" in request.form:
                    folder_name = request.form.get('default_album')
                    url_displays = [os.path.join(folder_name, i) for i in os.listdir(folder_name)]
                    session.permanent = True
                    session["folder"] = folder_name
                else:

                    folder = request.files.getlist("directory")
                    files_name = folder[0].filename
                    # rename_folder
                    if 'uploadedfolder' in request.form:
                        folder_name = request.form.get('uploadedfolder')
                    else:    
                        folder_name = files_name.split('/')[0]

                    session.permanent = True
                    session["folder"] = folder_name

                    try:
                        if not os.path.exists(f'./static/uploaded/{folder_name}'):
                            os.mkdir(f'./static/uploaded/{folder_name}')
                        else:
                            folder_name = folder_name+'_'+str(time())
                            os.mkdir(f'./static/uploaded/{folder_name}')
                    except Exception as e:
                            log.warning(e)
                    for i in folder:
                        name_i = i.filename
                        
                        if i.content_type == 'image/jpeg' or i.content_type == "image/png":
                            x = f'./static/uploaded/{folder_name}/{name_i.split("/")[-1]}'
                            url_displays.append(x[1:])
                            i.save(x)
                        else:
                            print(i.content_type)
                        
                        
                        
                if "faces" in session:
                    faces = session["faces"]
                album_details = session.get("album_details", {})
                folder_display = f'./static/uploaded/{folder_name}'
                # print("folder_name", folder_name)
                # print(session)
                return render_template('index.html',
                                        faces=faces,
                                        url_displays=url_displays,
                                        sizeFolder = sizeFolder,
                                        folder=folder_name,
                                        imgs_client_path=imgs_client_path,
                                        sizeFolderOrigin=len(os.listdir("./static/imgs/")),
                                        album_details=album_details,
                                        folder_display=folder_display)
        
        

    return redirect(url_for('homepage'))

@server.route('/api/delete-imgs/', methods=["POST"])
def apiDelete():
    session.permanent = True
    if "login" in session and session["login"] == 1:
        if request.method == "POST":
            file_delete = request.values["img_name_deleted"]
            log.warning(file_delete)
            log.warning("user")
            log.warning(session.get("email"))
            # request.remote_addr
            user_email = session.get("email")
            imgs_client_path = os.path.join("./static/client",user_email)
            if not os.path.exists(imgs_client_path):
                os.mkdir(imgs_client_path)


            # path = "./static/imgs"
            path = imgs_client_path
            # os.remove(imgs_client_path+'/'+file_delete+'.jpg')
            def rmv(lst = os.listdir(imgs_client_path), file_delete=file_delete+'.jpg'):
                os.remove(imgs_client_path+'/'+file_delete)
                lst.remove(file_delete)
                k = [int(i[4:].split(".jpg")[0]) for i in lst]
                
                j = k.copy()
                k.sort()
                # print(k)
                sort_lst = [lst[i] for i in [j.index(n) for n in k]]
                # print(sort_lst)
                # lst_new = len(lst)
                for i,j in enumerate(sort_lst):
                    if j == "img_"+str(i+1)+'.jpg':
                        continue
                    copyfile(imgs_client_path+'/'+j, imgs_client_path + '/'+"img_"+str(i+1)+'.jpg')
                    os.remove(imgs_client_path+'/'+j)
                    print(imgs_client_path + '/'+"img_"+str(i+1)+'.jpg')

            rmv()

            # reload_folder(path_name=imgs_client_path, dst_finally="hello")
            return jsonify(data={"valid":1})
    return redirect(url_for('homepage'))

            
@server.route('/face-generated/', methods = ["POST", "GET"])
def face_generated():
    session.permanent = True
    if "login" in session and session["login"] == 1:
        if request.method == "GET":
            if "faces" in session:
                faces = session["faces"]
            # log.warning(file_delete)
            log.warning("user")
            log.warning(session.get("email"))
            # request.remote_addr
            user_email = session.get("email")
            imgs_client_path = os.path.join("./static/client",user_email) # why : iframe
            sizeFolder = len(os.listdir(imgs_client_path))
            return render_template('generated.html', faces=faces, imgs_client_path=imgs_client_path[1:], sizeFolder = sizeFolder, sizeFolderOrigin=len(os.listdir("./static/imgs/")))

        if request.method == "POST":
            return redirect('/')
    return redirect(url_for('homepage'))


@server.route('/api/show-image/', methods = ["POST"])
def api_show_image():
    session.permanent = True
    if "login" in session and session["login"] == 1:
        if request.method == "POST":
            src1 = request.values["src1"]
            src2 = request.values["src2"]
            print(src1, src2)
            return jsonify(data = {
                "urls":[src1, src2]
            })
    return redirect(url_for('homepage'))

def reload_folder(path_name = "./static/imgs", dst_finally="img_"):
    for count, filename in enumerate(os.listdir(path_name)): 
        dst = dst_finally + str(count+1) + ".jpg"
        src = path_name+'/'+ filename
          
        # rename() function will 
        # rename all the files 
        os.rename(src, os.path.join(path_name,dst))
    if dst_finally == "img_":
        return 1
    return reload_folder(path_name = path_name, dst_finally="img_")
    

if __name__ == "__main__":
    db.create_all()
    server.run(host = '0.0.0.0', port=SERVER_PORT, debug=1)