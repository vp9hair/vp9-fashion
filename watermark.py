from PIL import Image, ImageDraw, ImageFont
import math
import numpy as np

def watermark(img_path):
    base = Image.open(img_path).convert('RGBA')
    width, height = base.size
    dim = width + height

    txt = Image.new('RGBA', base.size, (255,255,255,0))
    fnt = ImageFont.truetype('arial.ttf', int(dim/40*2)) 
    d = ImageDraw.Draw(txt)
    if height>width:
        d.text((0,height/2.25), "NONAVEL.COM", font=fnt, fill=(0,0,0,50))
    else:
        d.text((width/5,height/2.25), "NONAVEL.COM", font=fnt, fill=(0,0,0,50))
    txt = txt.rotate((math.atan(height/width)/3.14*180))    
    # else: 
    #     txt = txt.rotate((math.atan(width/height)/3.14*180))

    fnt = ImageFont.truetype('arial.ttf', int(dim/40)) 
    d = ImageDraw.Draw(txt)
    d.text((0,0), "nonavel.com", font=fnt, fill=(200,200,200,200))
    out = Image.alpha_composite(base, txt)
    out = np.asarray(out)
    return out
