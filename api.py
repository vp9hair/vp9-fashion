import sys
import torch
sys.path.append('./face_modules/')
import torchvision.transforms as transforms
import torch.nn.functional as F
from face_modules.model import Backbone, Arcface, MobileFaceNet, Am_softmax, l2_norm
from network.AEI_Net import *
from face_modules.mtcnn import *
import cv2
import PIL.Image as Image
import numpy as np
import configparser
from logging import warning

def cv2_imshow(x):
    cv2.imshow(x)
    cv2.waitKey(0)

# def load_stage1():
detector = MTCNN()
device = torch.device('cuda')
G = AEI_Net(c_id=512)

# print(G.summary)
G.eval()
G.load_state_dict(torch.load('./saved_models/G_latest.pth', map_location=torch.device('cuda')))
G = G.cuda()

arcface = Backbone(50, 1, 'ir_se').to(device)
arcface.eval()
arcface.load_state_dict(torch.load('./face_modules/model_ir_se50.pth', map_location=device), strict=False)

test_transform = transforms.Compose([
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
])

def crop_face(img_path):
    Xt_raw = cv2.imread(img_path)
    try:
        Xt, _ = detector.align(Image.fromarray(Xt_raw[:, :, ::-1]), crop_size=(256, 256), return_trans_inv=True)
        print(img_path, Xt, type(Xt))
        cv2.imwrite(img_path, cv2.cvtColor(np.asarray(Xt), cv2.COLOR_BGR2RGB))
        return Xt
    except Exception as e:
        warning(e)
        warning("!! Detect failed, check the image again!")

def action(source_img_name, target_img_name):
    Xs_raw = cv2.imread(source_img_name)
    # print(type(Xs_raw))
    try:
        Xs = detector.align(Image.fromarray(Xs_raw[:, :, ::-1]), crop_size=(256, 256))
    except Exception as e:
        print('the source image is wrong, please change the image')
    Xs_raw = np.array(Xs)[:, :, ::-1]
    Xs = test_transform(Xs)
    Xs = Xs.unsqueeze(0).cuda()

    with torch.no_grad():
        embeds = arcface(F.interpolate(Xs[:, :, 19:237, 19:237], (112, 112), mode='bilinear', align_corners=True))

    Xt_raw = cv2.imread(target_img_name)
    try:
        Xt, trans_inv = detector.align(Image.fromarray(Xt_raw[:, :, ::-1]), crop_size=(256, 256), return_trans_inv=True)
    except Exception as e:
        print('the target image is wrong, please change the image')
    Xt_raw = Xt_raw.astype(np.float)/255.0
    Xt = test_transform(Xt)
    Xt = Xt.unsqueeze(0).cuda()

    mask = np.zeros([256, 256], dtype=np.float)
    for i in range(256):
        for j in range(256):
            dist = np.sqrt(((i-128)**2 + (j-128)**2))/128
            dist = np.minimum(dist,1)
            mask[i, j] = 1-1*dist

    mask = cv2.dilate(mask, None, iterations=20)

    with torch.no_grad():
        Yt, _ = G(Xt, embeds)
        Yt = Yt.squeeze().detach().cpu().numpy().transpose([1, 2, 0])*0.5+0.5
        Yt = Yt[:, :, ::-1]
        Yt_trans_inv = cv2.warpAffine(Yt, trans_inv, (np.size(Xt_raw, 1), np.size(Xt_raw, 0)), borderValue=(0, 0, 0))
        mask_ = cv2.warpAffine(mask,trans_inv, (np.size(Xt_raw, 1), np.size(Xt_raw, 0)), borderValue=(0, 0, 0))
        mask_ = np.expand_dims(mask_, 2)
        # print(type(Yt))
        # show(Yt*255)
        Yt_trans_inv = mask_*Yt_trans_inv + (1-mask_)*Xt_raw
        # print(Yt_trans_inv*255)
        return Yt_trans_inv*255


from flask import Flask, jsonify, request, session
from datetime import timedelta
from flask_cors import CORS
import os

swap_server = Flask(__name__)
CORS(swap_server)

from PIL import Image, ImageDraw, ImageFont
import math


swap_server.secret_key = "nhatlt"

swap_server.config['PERMANENT_SESSION_LIFETIME'] =  timedelta(minutes=5)



def watermark(img_path):
    base = Image.open(img_path).convert('RGBA')
    width, height = base.size
    dim = width + height

    txt = Image.new('RGBA', base.size, (255,255,255,0))
    fnt = ImageFont.truetype('arial.ttf', int(dim/40*2)) 
    d = ImageDraw.Draw(txt)
    if height>width:
        d.text((0,height/2.25), "NONAVEL.COM", font=fnt, fill=(0,0,0,50))
    else:
        d.text((width/5,height/2.25), "NONAVEL.COM", font=fnt, fill=(0,0,0,50))
    txt = txt.rotate((math.atan(height/width)/3.14*180))    
    # else: 
    #     txt = txt.rotate((math.atan(width/height)/3.14*180))

    fnt = ImageFont.truetype('arial.ttf', int(dim/40)) 
    d = ImageDraw.Draw(txt)
    d.text((0,0), "nonavel.com", font=fnt, fill=(200,200,200,200))
    out = Image.alpha_composite(base, txt)
    out = np.asarray(out)
    out = cv2.cvtColor(out, cv2.COLOR_BGR2RGB)
    return out

session_json = {}
@swap_server.route('/api/swapping/', methods = ["POST"])
def api():
    global session_json
    if request.method == "POST":
        path_sys = "/home/nhatltvp9/workdir/web"
        src_img = request.values["src_img"]

        src_path = os.path.join(path_sys, src_img)
        folder_name = request.values["folder_name"]

        folder_path = os.path.join(path_sys, folder_name)
        # print("nayla gi",src_path,folder_path)
        list_dst_imgs = os.listdir(folder_path)
        
        output_return = []
        output_to_download = []
        for i in list_dst_imgs:
            x = os.path.join(folder_path, i)
            ori = x.split("/web")[-1]
            try:
                img = action(src_path, x)
            except Exception as e:
                warning(e)
                continue
            try:
                if not os.path.exists(f'{path_sys}/static/swapped/{folder_name.split("/")[-1]}'):
                    os.mkdir(f'{path_sys}/static/swapped/{folder_name.split("/")[-1]}')
                if not os.path.exists(f'{path_sys}/swapped/{folder_name.split("/")[-1]}'):
                    os.mkdir(f'{path_sys}/swapped/{folder_name.split("/")[-1]}')
            except:
                pass
            name_save = f'{path_sys}/swapped/{folder_name.split("/")[-1]}/{src_img.split("/")[-1]}_{i}.jpg'

            name_save_watermarked = f'{path_sys}/static/swapped/{folder_name.split("/")[-1]}/{src_img.split("/")[-1]}_{i}.jpg'
            cv2.imwrite(name_save, img)
            out = watermark(name_save)
            cv2.imwrite(name_save_watermarked, out)
            output_to_download.append(name_save)
            output_return.append([f'/static{name_save_watermarked.split("static")[-1]}', ori])

        # save folder download
        # session.permanent = True
        folder_name = folder_name.split("/")[-1]
        session_json.update({folder_name: output_to_download})
        
        return jsonify(data = {
            'imgs': output_return
            })


@swap_server.route('/api/crop-face/', methods = ["POST"])
def api_cropface():
    if request.method == "POST":
        img_path = '/home/nhatltvp9/workdir/web/'+request.values["img_path"]
        crop_face(img_path)
        return jsonify(data=True)

@swap_server.route('/api/via-email/', methods=["POST"])
def api_via_email():
    global session_json
    if request.method == "POST":
        # print(session["3123123"])
        if "folder_name" in request.values:
            folder_name = request.values["folder_name"]
        # session.permanent = True
        if folder_name in session_json:
            print(session_json[folder_name])
            return jsonify(data={
                "imgs": session_json[folder_name],
            })
        return jsonify(data={
                "imgs": 1,
            })

if __name__ == "__main__":
    swap_server.run(host = "0.0.0.0", port=8081, debug=1)
            
